var currencyData = [
		{
			"country" : "AUD",
			"countryCode"  : "aud",
			"countryCurrency" : "Australian Dollars"
		},
		{
			"country" : "CAD",
			"countryCode"  : "cad",
			"countryCurrency" : "Canadian Dollars"
		},
		{
			"country" : "CNY",
			"countryCode"  : "cny",
			"countryCurrency" : "Chinese Yuan"
		},
		{
			"country" : "CZK",
			"countryCode"  : "czk",
			"countryCurrency" : "Czech Republic Koruna"
		},
		{
			"country" : "DKK",
			"countryCode"  : "dkk",
			"countryCurrency" : "Danish Kroner"
		},
		{
			"country" : "EUR",
			"countryCode"  : "eur",
			"countryCurrency" : "European Union Euros"
		},
		{
			"country" : "GBP",
			"countryCode"  : "gbp",
			"countryCurrency" : "British Pounds"
		},
		{
			"country" : "JPY",
			"countryCode"  : "jpy",
			"countryCurrency" : "Japanese Yen"
		},
		{
			"country" : "NOK",
			"countryCode"  : "nok",
			"countryCurrency" : "Norwegian Kroner"
		},
		{
			"country" : "NZD",
			"countryCode"  : "nzd",
			"countryCurrency" : "New Zealand Dollars"
		},
		{
			"country" : "USD",
			"countryCode"  : "usd",
			"countryCurrency" : "United States Dollars"
		}
	];
