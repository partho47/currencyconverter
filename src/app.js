var toAmountElem;
var fromAmountElem;

var curbaseElem;
var curBaseCodeElem;
var fromFlagDisplayElem;

var baseCodeDisplayElem;
var termCodeDisplayElem;

var rateToElem;
var codeFmElem;
var codeToElem;

var curTermCodeElem;
var curConvertedElem;
var toFlagDisplayElem;

var twoDecimal = 2;
var zeroDecimal = 0;
var fourDecimal = 4;

var possibleConversionType = new Set(["1", "D", "Inv"]);

window.addEventListener('DOMContentLoaded', (event) => {
    var availableCurreny = currencyData;
    
	fromAmountElem = document.getElementById('curBase');
    toAmountElem = document.getElementById('curConverted');
	
    baseCodeDisplayElem = document.getElementById('baseCodeDisplay');
    termCodeDisplayElem = document.getElementById('termCodeDisplay');
	
	curbaseElem = document.getElementById('cur-base');
    curBaseCodeElem = document.getElementById('curBaseCode');
    fromFlagDisplayElem = document.getElementById('fromFlagDisplay');
	
	rateToElem = document.getElementsByClassName("rate-to");
	codeFmElem = document.getElementsByClassName("code-fm");
	codeToElem = document.getElementsByClassName("code-to");
	
    curTermCodeElem = document.getElementById('curTermCode');
	curConvertedElem = document.getElementById('cur-converted');
    toFlagDisplayElem = document.getElementById('toFlagDisplay');
	

    function currencyDropDownConst(element) {
        for (i = 0; i < availableCurreny.length; i++) {
            var elem = document.createElement('li');
            elem.value = availableCurreny[i].countryCode;
            elem.className = "dropdown-option-curr dropdown-option";
            elem.innerText = availableCurreny[i].countryCurrency;

            var newAttr = document.createAttribute("data-text");
            newAttr.value = availableCurreny[i].countryCurrency;
            elem.setAttributeNode(newAttr);

            var div = document.createElement("div");
            div.className = "flag" + " " + availableCurreny[i].countryCode;
            elem.appendChild(div);

            var span = document.createElement("span");
            span.className = "country-code";
            span.innerText = availableCurreny[i].country;

            elem.appendChild(span);
            element.appendChild(elem);
        }
    };

    currencyDropDownConst(baseCodeDisplayElem);
    currencyDropDownConst(termCodeDisplayElem);

    baseCodeDisplayElem.addEventListener("click", function(e) {
		toAmountElem.value = zeroDecimal;
		fromAmountElem.value = zeroDecimal;
        curbaseElem.innerText = e.target.dataset.text;
        curBaseCodeElem.innerText = e.target.lastElementChild.innerText;
        fromFlagDisplayElem.className = e.target.firstElementChild.className;
        var rate = getRate();
        rateToElem[0].innerText = rate;
        codeToElem[0].innerText = curTermCodeElem.innerText;
        codeFmElem[0].innerText = e.target.lastElementChild.innerText;
    });

    termCodeDisplayElem.addEventListener("click", function(e) {
		toAmountElem.value = zeroDecimal;
		fromAmountElem.value = zeroDecimal;
        curConvertedElem.innerText = e.target.dataset.text;
        curTermCodeElem.innerText = e.target.lastElementChild.innerText;
        toFlagDisplayElem.className = e.target.firstElementChild.className;
        var rate = getRate();
        rateToElem[0].innerText = rate;
        codeFmElem[0].innerText = curBaseCodeElem.innerText;
        codeToElem[0].innerText = e.target.lastElementChild.innerText;
    });
});

function getRate(){
	var rates;
    var toAmountCurrency = curTermCodeElem.innerText;
    var fromAmountCurrency = curBaseCodeElem.innerText;
	
	var conversionType = data[fromAmountCurrency][toAmountCurrency];
	if (possibleConversionType.has(conversionType)) {
		rates = calculateUnityDirectInvRateAmount(toAmountCurrency, fromAmountCurrency, conversionType);
	} else {
		rates = calculateCrossRateAmount(toAmountCurrency, fromAmountCurrency, conversionType);
	}
	
	if(rates != "Not Available"){
		rates = rates.toFixed(fourDecimal)
	} 
	
	console.log("Final Rate" + rates);
    return rates;
};

function numericDot(e){
	var charCode = (e.which) ? e.which : e.keyCode;
	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) return false;
	if(e.target.id == 'curBase' && e.target.value.indexOf(".") != -1 && charCode == 46) return false;
	if(e.target.id == 'curConverted' && e.target.value.indexOf(".") != -1 && charCode == 46) return false;
	return true;
};

function convert(conversionOrigin) {
    var toAmount;
    var fromAmount;
	var toAmountCurrency;
    var fromAmountCurrency;
    var rates = rateToElem[0].innerText;
    
	if (conversionOrigin == "From" && fromAmountElem.value == "") {
        toAmountElem.value = zeroDecimal.toFixed(twoDecimal);
        return;
    } else if (conversionOrigin == "From" && fromAmountElem.value != "") {
        toAmount = toAmountElem;
        fromAmount = fromAmountElem;
		toAmountCurrency = curTermCodeElem.innerText;
        fromAmountCurrency = curBaseCodeElem.innerText;
	} else if (conversionOrigin == "To" && toAmountElem.value == "") {
        fromAmountElem.value = zeroDecimal.toFixed(twoDecimal);
        return;
    } else if (conversionOrigin == "To" && toAmountElem.value != "") {
        fromAmount = toAmountElem;
        toAmount = fromAmountElem;
		toAmountCurrency = curBaseCodeElem.innerText;
        fromAmountCurrency = curTermCodeElem.innerText;
	}    
	
	if((conversionOrigin == "From" || conversionOrigin == "To" ) && rates != "Not Available"){
		var format = toAmountCurrency == "JPY" ? zeroDecimal : twoDecimal;
		var fromAmount = fromAmount.value;
		fromAmount = allowDot(fromAmount);
		fromAmount = removeComa(fromAmount);
		var tempRate = conversionOrigin == "To" ? 1/rates : rates;
		toAmount.value = addComa((fromAmount * tempRate).toFixed(format));
	} else {
		toAmount.value = zeroDecimal.toFixed(twoDecimal);
	}
};

function addComa(value){
	return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

function removeComa(value){
	var temp =  value.toString().replace (/,/g, "");
	return parseFloat(temp);
};

function allowDot(value){
	if(value.indexOf(".") == 0 && value.indexOf(".") == value.length-1){
		return zeroDecimal;
	} else if(value.indexOf(".") == value.length-1){
	   return value = value.substring(0, value.length - 1);
	} else{
	   return value;
	}
};
function calculateUnityDirectInvRateAmount(toAmountCurrency, fromAmountCurrency, conversionType) {
    switch (conversionType) {
        case "1":
            return 1;
            break;
        case "D":
            return calculateDirectRates(fromAmountCurrency, toAmountCurrency);
            break;
        case "Inv":
            return calculateInvertedRates(toAmountCurrency, fromAmountCurrency);
            break;
        default:
    }
}

function calculateDirectRates(fromAmountCurrency, toAmountCurrency) {
    return rate[fromAmountCurrency + toAmountCurrency];
};

function calculateInvertedRates(toAmountCurrency, fromAmountCurrency) {
    return 1 / rate[toAmountCurrency + fromAmountCurrency];
};

function calculateCrossRateAmount(toAmountCurrency, fromAmountCurrency, conversionType) {
    var finalRate = 1;
    var initialArray = [];
    initialArray.push(fromAmountCurrency);
    initialArray.push(conversionType);
    initialArray.push(toAmountCurrency);

    for (var i = 0; i < initialArray.length - 1; i++) {
        var initialLength = initialArray.length;
        var subCrossConversionType = data[initialArray[i]][initialArray[i + 1]];
        if (possibleConversionType.has(subCrossConversionType)) {
            var temp = calculateUnityDirectInvRateAmount(initialArray[i + 1], initialArray[i], subCrossConversionType);
            console.log("subCrossConversionType::" + subCrossConversionType + "::temp Rate::" + temp);
			if(temp == undefined){
				finalRate = "Not Available"; 
				break;
			}else{
				finalRate = finalRate * temp;
				continue;
			}
        }
        initialArray.splice(i + 1, 0, subCrossConversionType);
        if (initialArray.length != initialLength) {
            i = 0;
            i--;
            finalRate = 1;
            console.log("Reset everything and start over");
        }
    }
	return finalRate;
};